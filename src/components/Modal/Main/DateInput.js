import React, { Component } from 'react';

class DateInput extends Component {
  
  render() {
    return (
      <div className='date-input'>
        <input type='date' placeholder='100' value={this.props.field} 
        onChange={(event) => this.props.update(event.target.value)}></input>
      </div>
    )
  }
}

export default DateInput;
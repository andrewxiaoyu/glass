import React, { Component } from 'react';
import { HelpCircle } from 'react-feather';
import ReactTooltip from 'react-tooltip';
import { connect } from 'react-redux';

class Amounts extends Component {
  render() {
    return (
      <div id='modal-amounts'>
        <div className='modal-row'>
          <h4>Amounts
            <a data-tip='Amount for various time intervals' data-for='name'><HelpCircle /></a>
            <ReactTooltip id='name' className='tooltip' effect='solid'/>
          </h4>
        </div>
        <div className='modal-row'>
          <h4>Every Day</h4>
          <span>${this.props.day.toFixed(2)}</span>
        </div>
        <div className='modal-row'>
          <h4>Every Week</h4>
          <span>${this.props.week.toFixed(2)}</span>
        </div>
        <div className='modal-row'>
          <h4>Every Month</h4>
          <span>${this.props.month.toFixed(2)}</span>
        </div>
        <div className='modal-row'>
          <h4>Every Year</h4>
          <span>${this.props.year.toFixed(2)}</span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    ...state.modal.item.amounts
  };
};

export default connect(mapStateToProps, null)(Amounts);
import React, { Component } from 'react';

class IntervalDropdown extends Component {
  render() {
    return (
      <select value={this.props.field} 
      onChange={(event) => this.props.update(event.target.value)}>
        <option value="once">One Time</option>
        <option value="day">Daily</option>
        <option value="week">Weekly</option>
        <option value="biweek">Biweekly</option>
        <option value="semimonth">Semi-monthly</option>
        <option value="month">Monthly</option>
        <option value="semiyear">Semi-annually</option>
        <option value="year">Annually</option>
      </select>
    )
  }
}

export default IntervalDropdown;
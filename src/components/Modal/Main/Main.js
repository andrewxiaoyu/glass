import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Income,
  Bills,
  Debt
} from './Types';

class Main extends Component {
  render() {
    switch(this.props.role){
      case 'income':{
        return (
          <Income />
        );
      }
      case 'bills':{
        return (
          <Bills />
        );
      }
      case 'debt':{
        return (
          <Debt />
        );
      }
    }
    return null;
  }
}

const mapStateToProps = state => {
  return {
    role: state.modal.role
  };
};

export default connect(mapStateToProps, null)(Main);
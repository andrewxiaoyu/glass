import React, { Component } from 'react';

class MoneyInput extends Component {
  
  render() {
    return (
      <div className='money-input'>
        <span>$</span>
        <input type='number' placeholder='100' value={this.props.field} 
        onChange={(event) => this.props.update(event.target.value)}></input>
      </div>
    )
  }
}

export default MoneyInput;
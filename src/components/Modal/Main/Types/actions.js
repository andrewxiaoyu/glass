export function setName(value){
  return {
    type: 'MODAL_SET_NAME',
    payload: value
  }
}

export function setAmount(value){
  return {
    type: 'MODAL_SET_AMOUNT',
    payload: parseFloat(value)
  }
}

export function updateAmount(value){
  return function (dispatch) {
    dispatch(setAmount(value));
    dispatch(updateAmounts());
  }
}

export function setFrequency(value){
  return {
    type: 'MODAL_SET_FREQUENCY',
    payload: value
  }
}

export function updateFrequency(value){
  return function (dispatch) {
    dispatch(setFrequency(value));

    if ( value === 'once' ) dispatch(setIncomeFreq('once'));

    dispatch(updateAmountFromIncome());
    dispatch(updateAmounts());
  }
}

export function setDate(value){
  return {
    type: 'MODAL_SET_DATE',
    payload: value
  }
}

export function updateDate(value){
  return function (dispatch) {
    dispatch(setDate(value));
  }
}

export function setTax(value){
  return {
    type: 'MODAL_SET_TAX',
    payload: parseFloat(value)
  }
}

export function updateTax(value){
  return function (dispatch) {
    dispatch(setTax(value));
    dispatch(updateAmountFromIncome());
    dispatch(updateAmounts());
  }
}

export function setIncome(value){
  return {
    type: 'MODAL_SET_INCOME',
    payload: parseFloat(value)
  }
}

export function updateIncome(value){
  return function (dispatch) {
    dispatch(setIncome(value));
    dispatch(updateAmountFromIncome());
    dispatch(updateAmounts());
  }
}

export function setIncomeFreq(value){
  return {
    type: 'MODAL_SET_INCOME_FREQ',
    payload: value
  }
}

export function updateIncomeFreq(value){
  return function (dispatch) {
    dispatch(setIncomeFreq(value));

    if ( value === 'once' ) dispatch(setFrequency('once'));
    else dispatch(setFrequency('day'));

    dispatch(updateAmountFromIncome());
    dispatch(updateAmounts());
  }
}

export function setAmounts(value){
  return {
    type: 'MODAL_SET_AMOUNTS',
    payload: value
  }
}

export function updateAmountFromIncome(){
  return function (dispatch, getState) {
    let { item } = getState().modal;
    if(item.income && item.tax && item.incomefreq && item.frequency) {
      var amount = item.income; 
      switch(item.incomefreq) { // get yearly salary
        case 'day': {
          amount *= 365;
          break;
        }
        case 'week': {
          amount *= 52;
          break;
        }
        case 'biweek': {
          amount *= 26;
          break;
        }
        case 'semimonth': {
          amount *= 24;
          break;
        }
        case 'month': {
          amount *= 12;
          break;
        }
        case 'semiyear': {
          amount *= 2;
          break;
        }
        default: {
          amount *= 1;
        }
      }
      switch(item.frequency) { // get gross income based on freq
        case 'day': {
          amount /= 365;
          break;
        }
        case 'week': {
          amount /= 52;
          break;
        }
        case 'biweek': {
          amount /= 26;
          break;
        }
        case 'semimonth': {
          amount /= 24;
          break;
        }
        case 'month': {
          amount /= 12;
          break;
        }
        case 'semiyear': {
          amount /= 2;
          break;
        }
        default: {
          amount /= 1;
        }
      }

      amount *= (1 - item.tax/100);
      dispatch(setAmount(amount.toFixed(2)));
    }
  }
}

export function updateAmounts() {
  return function (dispatch, getState) {
    var { item } = getState().modal;
    var amounts = {};

    switch(item.frequency) {
      case 'day': {
        amounts = {
          day: item.amount * 1,
          week: item.amount * 7,
          month: item.amount * 30,
          year: item.amount * 365,
        }
        break;
      }
      case 'week': {
        amounts = {
          day: item.amount * 7,
          week: item.amount * 1,
          month: item.amount * 4,
          year: item.amount * 52,
        }
        break;
      }
      case 'biweek': {
        amounts = {
          day: item.amount / 14,
          week: item.amount / 2,
          month: item.amount * 2,
          year: item.amount * 26,
        }
        break;
      }
      case 'semimonth': {
        amounts = {
          day: item.amount / 15,
          week: item.amount / 2,
          month: item.amount * 2,
          year: item.amount * 24,
        }
        break;
      }
      case 'month': {
        amounts = {
          day: item.amount / 30,
          week: item.amount / 4,
          month: item.amount * 1,
          year: item.amount * 12,
        }
        break;
      }
      case 'semiyear': {
        amounts = {
          day: item.amount / 182,
          week: item.amount / 26,
          month: item.amount / 12,
          year: item.amount * 2,
        }
        break;
      }
      case 'year': {
        amounts = {
          day: item.amount / 365,
          week: item.amount / 52,
          month: item.amount / 12,
          year: item.amount * 1,
        }
        break;
      }
    }

    dispatch(setAmounts(amounts));
  }
}
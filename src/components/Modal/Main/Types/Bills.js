import React, { Component } from 'react';
import { HelpCircle } from 'react-feather';
import ReactTooltip from 'react-tooltip';
import { connect } from 'react-redux';

import MoneyInput from '../MoneyInput';
import IntervalDropdown from '../IntervalDropdown';
import DateInput from '../DateInput';
import Amounts from '../Amounts';
import { setName, updateAmount, updateFrequency, updateDate } from './actions';

class Bills extends Component {
  render() {
    return (
      <div id='modal-content'>
        <div id='modal-inputs'>
          <div className='modal-row'>
            <h4>Name
              <a data-tip='Where is this bill for?' data-for='name'><HelpCircle /></a>
              <ReactTooltip id='name' className='tooltip' effect='solid'/>
            </h4>
            <input type='text' placeholder='Name' value={this.props.item.name} 
            onChange={(event) => this.props.setName(event.target.value)}></input>
          </div>
          <div className='modal-row'>
            <h4>Bill Amount
              <a data-tip='How much do you pay?' data-for='bill-amount'><HelpCircle /></a>
              <ReactTooltip id='bill-amount' className='tooltip' effect='solid'/>
            </h4>
            <MoneyInput field={this.props.item.amount} update={this.props.updateAmount} />
          </div>
          <div className='modal-row'>
            <h4>Bill Frequency
              <a data-tip='How often do you pay this bill?' data-for='bill-freq'><HelpCircle /></a>
              <ReactTooltip id='bill-freq' className='tooltip' effect='solid'/>
            </h4>
            <IntervalDropdown field={this.props.item.frequency} update={this.props.updateFrequency} />
          </div>
          <div className='modal-row'>
            <h4>Debt Date
              <a data-tip='When do you pay this debt? Select the first date.' data-for='debt-date'><HelpCircle /></a>
              <ReactTooltip id='debt-date' className='tooltip' effect='solid'/>
            </h4>
            <DateInput field={this.props.item.date} update={this.props.updateDate} />
          </div>
        </div>
        { this.props.item.frequency !== 'once' && <Amounts /> }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    item: state.modal.item
  };
};

const mapDispatchToProps = {
  setName,
  updateAmount,
  updateFrequency,
  updateDate
};

export default connect(mapStateToProps, mapDispatchToProps)(Bills);
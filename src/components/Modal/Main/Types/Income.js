import React, { Component } from 'react';
import { HelpCircle } from 'react-feather';
import ReactTooltip from 'react-tooltip';
import { connect } from 'react-redux';

import MoneyInput from '../MoneyInput';
import IntervalDropdown from '../IntervalDropdown';
import DateInput from '../DateInput';
import Amounts from '../Amounts';
import { 
  setName,
  updateIncome,
  updateIncomeFreq,
  updateTax,
  updateAmount,
  updateFrequency,
  updateDate
} from './actions';

class Income extends Component {
  render() {
    return (
      <div id='modal-content'>
        <div id='modal-inputs'>
          <div className='modal-row'>
            <h4>Name
              <a data-tip='Where is this income from?' data-for='name'><HelpCircle /></a>
              <ReactTooltip id='name' className='tooltip' effect='solid'/>
            </h4>
            <input type='text' placeholder='Name' value={this.props.item.name} 
              onChange={(event) => this.props.setName(event.target.value)}></input>
          </div>

          <div className='modal-row'>
            <h4>Net Income
              <a data-tip='What is your salary?' data-for='net-income'><HelpCircle /></a>
              <ReactTooltip id='net-income' className='tooltip' effect='solid'/>
            </h4>
            <MoneyInput field={this.props.item.income} update={this.props.updateIncome} />
          </div>
          <div className='modal-row'>
            <h4>Income Frequencey
              <a data-tip='How long does it take for you to receive this income?' data-for='income-freq'><HelpCircle /></a>
              <ReactTooltip id='income-freq' className='tooltip' effect='solid'/>
            </h4>
            <IntervalDropdown field={this.props.item.incomefreq} update={this.props.updateIncomeFreq} />
          </div>
          <div className='modal-row'>
            <h4>Tax Rate
              <a data-tip='What is your tax rate on your net income?' data-for='income-tax'><HelpCircle /></a>
              <ReactTooltip id='income-tax' className='tooltip' effect='solid'/>
            </h4>
            <MoneyInput field={this.props.item.tax} update={this.props.updateTax} />
          </div>
          <div className='modal-row'>
            <h4>Gross Income
              <a data-tip='How much do you pocket?' data-for='payment-freq'><HelpCircle /></a>
              <ReactTooltip id='payment-freq' className='tooltip' effect='solid'/>
            </h4>
            <MoneyInput field={this.props.item.amount} update={this.props.updateAmount} />
          </div>
          { this.props.item.incomefreq !== 'once' && 
          <div className='modal-row'>
            <h4>Payment Frequency
              <a data-tip='How often do you receive income?' data-for='payment-freq'><HelpCircle /></a>
              <ReactTooltip id='payment-freq' className='tooltip' effect='solid'/>
            </h4>
            <IntervalDropdown field={this.props.item.frequency} update={this.props.updateFrequency} />
          </div>
          }
          <div className='modal-row'>
            <h4>Debt Date
              <a data-tip='When do you receive income? Select the first date.' data-for='payment-date'><HelpCircle /></a>
              <ReactTooltip id='payment-date' className='tooltip' effect='solid'/>
            </h4>
            <DateInput field={this.props.item.date} update={this.props.updateDate} />
          </div>
        </div>
        { this.props.item.frequency !== 'once' && this.props.item.incomefreq !== 'once' && <Amounts /> }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    item: state.modal.item
  };
};

const mapDispatchToProps = {
  setName, 
  updateIncome,
  updateIncomeFreq,
  updateTax,
  updateAmount,
  updateFrequency,
  updateDate
};

export default connect(mapStateToProps, mapDispatchToProps)(Income);
import React, { Component } from 'react';
import { XCircle } from 'react-feather';
import { connect } from 'react-redux';

import { hideModal } from './actions';

class Header extends Component {
  render() {
    return (
      <div id='modal-header'>
        <h2>{this.props.add ? "Add" : "Edit"} {this.props.role.charAt(0).toUpperCase() + this.props.role.slice(1)}</h2>
        <XCircle onClick={()=>this.props.hideModal()}/>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    add: state.modal.add,
    role: state.modal.role
  };
};

const mapDispatchToProps = {
  hideModal
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);

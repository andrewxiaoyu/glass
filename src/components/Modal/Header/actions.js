export function hideModal(){
  return {
    type: 'MODAL_HIDE',
    payload: true
  }
}
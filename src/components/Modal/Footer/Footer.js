import React, { Component } from 'react';
import { connect } from 'react-redux';

import { updateItem } from './actions';

class Footer extends Component {
  render() {
    return (
      <div id='modal-footer'>
        <button onClick={()=>this.props.updateItem()}>{this.props.add ? "Add" : "Save"}</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    add: state.modal.add
  };
};

const mapDispatchToProps = {
  updateItem
};

export default connect(mapStateToProps, mapDispatchToProps)(Footer);

export function updateItem(){
  return function (dispatch, getState) {
    let { role, add, item } = getState().modal;
    if (add){
      dispatch(addItem(role, item))
    }
    else
    {
      dispatch(editItem(role, item));
    }
    dispatch(hideModal());
  }
}

export function addItem(role, item){
  return {
    type: 'MONEY_ADD',
    payload: {
      role: role,
      item: item
    }
  }
}

export function editItem(role, item){
  return {
    type: 'MONEY_EDIT',
    payload: {
      role: role,
      item: item
    }
  }
}

export function hideModal(){
  return {
    type: 'MODAL_HIDE',
    payload: true
  }
}
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Header from './Header';
import Main from './Main';
import Footer from './Footer';

class Modal extends Component {
  render() {
    if( this.props.modal )
      return (
        <div id='modal-container'>
          <div id='modal'>
            <Header />
            <Main />
            <Footer />
          </div>
        </div>
      );
    return null;
  }
}

const mapStateToProps = state => {
  return {
    modal: state.modal.modal
  };
};

export default connect(mapStateToProps, null)(Modal);
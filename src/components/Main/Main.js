import React, { Component } from 'react';

import InputSection from './InputSection';
import OutputSection from './OutputSection';

class Main extends Component {
  render() {
    return (
      <div id='main-container'>
        <InputSection />
        <OutputSection />
      </div>
    );
  }
}

export default Main;

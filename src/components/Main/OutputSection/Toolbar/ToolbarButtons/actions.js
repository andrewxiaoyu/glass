export function setView(value){
    return {
        type: 'GRAPH_SET_VIEW',
        payload: value
    }
}
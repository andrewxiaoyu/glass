import React, { Component } from 'react';
import { connect } from 'react-redux';

import { setView } from './actions';

class ViewButton extends Component {
  render() {
    return (
    <button className={this.props.view === this.props.value ? 'selected' : ''}
    onClick={() => this.props.setView(this.props.value)}>
        {this.props.value.charAt(0).toUpperCase() + this.props.value.slice(1)} View
    </button>
    );
  }
}

const mapStateToProps = state => {
  return {
    view: state.graph.view
  };
};

const mapDispatchToProps = {
  setView
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewButton);

import React, { Component } from 'react';

import ViewButton from './ViewButton';

class ToolbarButtons extends Component {
  render() {
    return (
      <div id='toolbar-buttons'>
        <button>Reset to Default</button>
        <ViewButton value='individual' />
        <ViewButton value='combined' />
      </div>
    );
  }
}

export default ToolbarButtons;

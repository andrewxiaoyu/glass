export function setTimeframe(value){
    return {
        type: 'GRAPH_SET_TIMEFRAME',
        payload: value
    }
}
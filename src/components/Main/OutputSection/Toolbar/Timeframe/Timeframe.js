import React, { Component } from 'react';

import TimeframeButton from './TimeframeButton';

class Timeframe extends Component {
  render() {
    return (
      <div id="timerange">
        <TimeframeButton value={"1d"} />
        <TimeframeButton value={"1w"} />
        <TimeframeButton value={"1m"} />
        <TimeframeButton value={"6m"} />
        <TimeframeButton value={"1y"} />
      </div>
    );
  }
}

export default Timeframe;

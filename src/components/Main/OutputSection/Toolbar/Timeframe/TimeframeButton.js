import React, { Component } from 'react';
import { connect } from 'react-redux';

import { setTimeframe } from './actions';

class TimeframeButton extends Component {
  render() {
    return (
      <button className={this.props.timeframe === this.props.value ? 'selected' : ''} 
      onClick={() => this.props.setTimeframe(this.props.value)}>
        { this.props.value }
      </button>
    );
  }
}

const mapStateToProps = state => {
  return {
    timeframe: state.graph.timeframe
  };
};

const mapDispatchToProps = {
  setTimeframe
};

export default connect(mapStateToProps, mapDispatchToProps)(TimeframeButton);

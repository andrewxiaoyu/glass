import React, { Component } from 'react';

import ToolbarButtons from './ToolbarButtons';
import Timeframe from './Timeframe';

class Toolbar extends Component {
  render() {
    return (
      <div id="toolbar">
        <ToolbarButtons />
        <Timeframe />
      </div>
    );
  }
}

export default Toolbar;

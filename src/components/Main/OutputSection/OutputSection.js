import React, { Component } from 'react';

import Toolbar from './Toolbar';

class OutputSection extends Component {
  render() {
    return (
      <div id="output-section">
        <Toolbar />
      </div>
    );
  }
}

export default OutputSection;

import React, { Component } from 'react';

import Balance from './Balance';
import Savings from './Savings';
import {
  Income,
  Bills,
  Debt
} from './Input/Types';

class InputSection extends Component {
  render() {
    return (
      <div id="input-section">
        <Balance />
        <Savings />
        <Income />
        <Bills />
        <Debt />
      </div>
    );
  }
}

export default InputSection;

import React, { Component } from 'react';
import { HelpCircle } from 'react-feather';
import ReactTooltip from 'react-tooltip';
import { connect } from 'react-redux';

import { updateSavings } from './actions';

class Savings extends Component {
  render() {
    return (
      <div className='input-container'>
        <h3>My Savings
          <a data-tip='What is your savings rate from your annual income?' data-for='name'><HelpCircle /></a>
          <ReactTooltip id='name' className='tooltip' effect='solid'/>
        </h3>
        <div className='input-money input-savings'>
          <input type='range' min='0' max='100' id='savings-range'
          value={this.props.savings} onChange={(event) => this.props.updateSavings(event.target.value)} />
          <div className='input-savings-perc'>
            <input type='number' placeholder='20' 
            value={this.props.savings} onChange={(event) => this.props.updateSavings(event.target.value)} />
            <span>%</span>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    savings: state.money.savings
  };
};

const mapDispatchToProps = {
  updateSavings
};

export default connect(mapStateToProps, mapDispatchToProps)(Savings);
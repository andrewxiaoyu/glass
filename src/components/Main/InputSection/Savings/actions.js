export function setSavings(value){
  return {
    type: 'MONEY_SET_SAVINGS',
    payload: value
  }
}

export function updateSavings(value){
  return function (dispatch) {
    value = Math.abs(value);
    value = value > 100 ? 100 : value;
    dispatch(setSavings(value));
  }
}
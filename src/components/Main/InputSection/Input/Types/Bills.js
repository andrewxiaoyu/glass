import React, { Component } from 'react';
import { connect } from 'react-redux';

import InputContainer from '../InputContainer';
import { addBill, editBill } from './actions';

class Bills extends Component {

  _add(){
    this.props.addBill();
  }
  _edit(item){
    this.props.editBill(item)
  }

  render() {
    return (
      <InputContainer
        role='bills'
        items={
          this.props.bills
        }
        add={()=>this._add()}
        edit={(item)=>this._edit(item)}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    bills: state.money.bills
  };
};

const mapDispatchToProps = {
  addBill,
  editBill
};

export default connect(mapStateToProps, mapDispatchToProps)(Bills);
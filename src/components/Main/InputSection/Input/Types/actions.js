export function addIncome(){
  let item = {
    role: 'income',
    add: true,
    item: {
      name: '',
      amount: 100,
      frequency: 'once',
      
      income: 100,
      incomefreq: 'once',
      tax: 10,

      amounts: {
        day: null,
        week: null,
        month: null,
        year: null,
      }
    }
  }

  return {
    type: 'MODAL_SHOW',
    payload: item
  }
}

export function editIncome(item){
  let payload = {
    role: 'income',
    add: false,
    item: item
  }

  return {
    type: 'MODAL_SHOW',
    payload: payload
  }
}

export function addBill(){
  let item = {
    role: 'bills',
    add: true,
    item: {
      name: '',
      amount: 100,
      frequency: 'once',

      amounts: {
        day: null,
        week: null,
        month: null,
        year: null,
      }
    }
  }

  return {
    type: 'MODAL_SHOW',
    payload: item
  }
}

export function editBill(item){
  let payload = {
    role: 'bills',
    add: false,
    item: item
  }

  return {
    type: 'MODAL_SHOW',
    payload: payload
  }
}

export function addDebt(){
  let item = {
    role: 'debt',
    add: true,
    item: {
      name: '',
      amount: 100,
      frequency: 'once',

      amounts: {
        day: null,
        week: null,
        month: null,
        year: null,
      }
    }
  }

  return {
    type: 'MODAL_SHOW',
    payload: item
  }
}

export function editDebt(item){
  let payload = {
    role: 'debt',
    add: false,
    item: item
  }

  return {
    type: 'MODAL_SHOW',
    payload: payload
  }
}
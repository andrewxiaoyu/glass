import React, { Component } from 'react';
import { connect } from 'react-redux';

import InputContainer from '../InputContainer';
import { addIncome, editIncome } from './actions';

class Income extends Component {

  _add(){
    this.props.addIncome();
  }
  _edit(item){
    this.props.editIncome(item)
  }
  render() {
    return (
      <InputContainer 
        role='income'
        items={
          this.props.income
        }
        add={()=>this._add()}
        edit={(item)=>this._edit(item)}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    income: state.money.income
  };
};

const mapDispatchToProps = {
  addIncome,
  editIncome
};

export default connect(mapStateToProps, mapDispatchToProps)(Income);
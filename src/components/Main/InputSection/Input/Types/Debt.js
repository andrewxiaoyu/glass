import React, { Component } from 'react';
import { connect } from 'react-redux';

import InputContainer from '../InputContainer';
import { addDebt, editDebt } from './actions';

class Debt extends Component {

  _add(){
    this.props.addDebt();
  }
  _edit(item){
    this.props.editDebt(item)
  }

  render() {
    return (
      <InputContainer
        role='debt'
        items={
          this.props.debt
        }
        add={()=>this._add()}
        edit={(item)=>this._edit(item)}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    debt: state.money.debt
  };
};

const mapDispatchToProps = {
  addDebt,
  editDebt
};

export default connect(mapStateToProps, mapDispatchToProps)(Debt);
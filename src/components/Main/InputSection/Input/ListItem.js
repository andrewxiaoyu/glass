import React, { Component } from 'react';
import { Repeat, XCircle } from 'react-feather';
import { connect } from 'react-redux';

import { removeItem } from './actions';

class ListItem extends Component {
  render() {
    const repeat = this.props.frequency !== 'once' ? <Repeat /> : <span></span>

    return (
      <li key={this.props.id}>
        <div className='item-header'>
          <h3 onClick={()=>this.props.edit()}>{this.props.name}</h3>
          { repeat }
          <XCircle onClick={()=>this.props.removeItem(this.props.role, this.props.id)}/>
        </div>
        <div className='item-content' onClick={()=>this.props.edit()}>
          <p>{this.props.amount}</p>
          <p className='align-right'>{this.props.frequency}</p>
        </div>
      </li>
    );
  }
}

const mapDispatchToProps = {
  removeItem
};

export default connect(null, mapDispatchToProps)(ListItem);
export function removeItem(role, key){
  return {
    type: 'MONEY_REMOVE',
    payload: {
      role: role,
      id: key
    }
  }
}
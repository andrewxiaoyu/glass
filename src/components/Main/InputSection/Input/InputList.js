import React, { Component } from 'react';
import ListItem from './ListItem';

function InputList (props){
  const  { items } = props;
  if(items && items.length > 0)
  {
    const listItems = items.map((item) => 
      <ListItem key={item.id} role={props.role} edit={()=>props.edit(item)} {...item} />
    )
    return (
      <ul className='input-list'>{listItems}</ul>
    );
  }
  return null;
}

export default InputList;

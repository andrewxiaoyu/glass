import React, { Component } from 'react';
import InputList from './InputList';
import { PlusCircle } from 'react-feather';

class InputContainer extends Component {
  render() {
    const list = this.props.items.length > 0 ? 
      <InputList role={this.props.role} items={this.props.items} edit={(item)=>this.props.edit(item)} /> : 
      <p>No {this.props.role}</p> ;
    return (
      <div className='input-container'>
        <div className='input-header'>
          <h3>{this.props.role.charAt(0).toUpperCase() + this.props.role.slice(1)}</h3>
          <PlusCircle onClick={()=>this.props.add()}/>
        </div>
        {list}
      </div>
    );
  }
}

export default InputContainer;

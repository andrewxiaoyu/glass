export function setBalance(value){
  return {
    type: 'MONEY_SET_BALANCE',
    payload: value
  }
}

export function updateBalance(value){
  return function (dispatch) {
    dispatch(setBalance(value));
  }
}
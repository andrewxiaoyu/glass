import React, { Component } from 'react';
import { HelpCircle } from 'react-feather';
import ReactTooltip from 'react-tooltip';
import { connect } from 'react-redux';

import { updateBalance } from './actions';

class Balance extends Component {
  render() {
    return (
      <div className='input-container'>
        <h3>My Balance
          <a data-tip='What is your current balance?' data-for='name'><HelpCircle /></a>
          <ReactTooltip id='name' className='tooltip' effect='solid'/>
        </h3>
        <div className='input-money input-balance'>
          <span>$</span>
          <input type='number' placeholder='100' 
          value={this.props.balance} onChange={(event) => this.props.updateBalance(event.target.value)} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    balance: state.money.balance
  };
};

const mapDispatchToProps = {
  updateBalance
};

export default connect(mapStateToProps, mapDispatchToProps)(Balance);
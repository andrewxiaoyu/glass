import React, { Component } from 'react';

import Modal from './Modal';
import Header from './Header';
import Footer from './Footer';
import Main from './Main';

class AppLayout extends Component {
  render() {
    return (
      <div>
        <Modal />
        <Header />
        <Main />
        <Footer />
      </div>
    );
  }
}

export default AppLayout;
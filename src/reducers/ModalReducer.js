const initialState = {
  modal: false,
  role: null,
  add: true,
  item: {
    name: 'Groceries',
    amount: 100,
    frequency: 'once',
    date: null,
    
    income: 100,
    incomefreq: 'once',
    tax: 10,

    amounts: {
      day: null,
      week: null,
      month: null,
      year: null,
    }
  }
}

export default function reducer(state = initialState, action) {
  switch(action.type) {
    case 'MODAL_SHOW': {
      return {
        ...state,
        modal: true,
        ...action.payload
      }
    }
    case 'MODAL_HIDE': {
      return {
        ...state,
        modal: false,
        role: null,
        item: null
      }
    }
    case 'MODAL_SET_NAME': {
      let newState = JSON.parse(JSON.stringify(state));
      newState.item.name = action.payload;
      return newState;
    }
    case 'MODAL_SET_AMOUNT': {
      let newState = JSON.parse(JSON.stringify(state));
      newState.item.amount = action.payload;
      return newState;
    }
    case 'MODAL_SET_FREQUENCY': {
      let newState = JSON.parse(JSON.stringify(state));
      newState.item.frequency = action.payload;
      return newState;
    }
    case 'MODAL_SET_DATE': {
      let newState = JSON.parse(JSON.stringify(state));
      newState.item.date = action.payload;
      return newState;
    }
    case 'MODAL_SET_INCOME': {
      let newState = JSON.parse(JSON.stringify(state));
      newState.item.income = action.payload;
      return newState;
    }
    case 'MODAL_SET_INCOME_FREQ': {
      let newState = JSON.parse(JSON.stringify(state));
      newState.item.incomefreq = action.payload;
      return newState;
    }
    case 'MODAL_SET_TAX': {
      let newState = JSON.parse(JSON.stringify(state));
      newState.item.tax = action.payload;
      return newState;
    }
    case 'MODAL_SET_AMOUNTS': {
      let newState = JSON.parse(JSON.stringify(state));
      newState.item.amounts = action.payload;
      return newState;
    }
  }
  return state;
}
const initialState = {
    view: 'individual',
    timeframe: '1d',
  }
  
  export default function reducer(state = initialState, action) {
    switch(action.type) {
      case 'GRAPH_SET_VIEW': {
        return {
          ...state,
          view: action.payload
        }
      }
      case 'GRAPH_SET_TIMEFRAME': {
        return {
          ...state,
          timeframe: action.payload
        }
      }
    }
    return state;
  }
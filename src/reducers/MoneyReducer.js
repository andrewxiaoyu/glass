import uniqid from 'uniqid';

const initialState = {
  balance: 100,
  savings: 20,
  income: [],
  bills: [],
  debt: [],
}

export default function reducer(state = initialState, action) {
  switch(action.type) {
    case 'MONEY_SET_BALANCE': {
      return {
        ...state,
        balance: action.payload
      }
    }
    case 'MONEY_SET_SAVINGS': {
      return {
        ...state,
        savings: action.payload
      }
    }
    case 'MONEY_ADD': {
      let newState = JSON.parse(JSON.stringify(state));
      let item = action.payload.item;
      item.id = uniqid();
      newState[action.payload.role].push(item);
      return newState;
    }
    case 'MONEY_EDIT': {
      let newState = JSON.parse(JSON.stringify(state));
      let item = action.payload.item;
      let found = newState[action.payload.role].findIndex((i)=>{return i.id === item.id});
      newState[action.payload.role][found] = item;
      return newState;
    }
    case 'MONEY_REMOVE': {
      let newState = JSON.parse(JSON.stringify(state));
      let found = newState[action.payload.role].findIndex((i)=>{return i.id === action.payload.id});
      found >= 0 ? newState[action.payload.role].splice(found, 1) : false;
      return newState;
    }
  }
  return state;
}
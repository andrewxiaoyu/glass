import { combineReducers } from 'redux';

import modal from './ModalReducer';
import money from './MoneyReducer';
import graph from './GraphReducer';

export default combineReducers({
  modal,
  money,
  graph
})